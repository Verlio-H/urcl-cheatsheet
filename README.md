
# URCL Cheatsheet

| NAME   | Y    | A    | B    | TYPE    | C EQUIVALENT                    | FULL NAME |
| ------ | ---- | ---- | ---- | ------- | ------------------------------- | --------- |
| ABS    | REG  | SNUM |      | BASIC   | `Y = (A < 0) ? -A : A;`         | Absolute |
| ADD    | REG  | NUM  | NUM  | CORE    | `Y = A + B;`                    | Add |
| AND    | REG  | NUM  | NUM  | BASIC   | `Y = A & B;`                    | Bitwise AND |
| BEV    | ADDR | NUM  |      | BASIC   | `if (A & 0x1 != 0) PC = Y;`     | Branch if even |
| BGE    | ADDR | NUM  | NUM  | CORE    | `if (A >= B) PC = Y;`           | Branch if greater than or equal to |
| BRC    | ADDR | NUM  | NUM  | BASIC   | `if (A + B > MAX) PC = Y;`      | Branch if carry |
| BRE    | ADDR | NUM  | NUM  | BASIC   | `if (A == B) PC = Y;`           | Branch if equal |
| BRG    | ADDR | NUM  | NUM  | BASIC   | `if (A > B) PC = Y;`            | Branch if greater than |
| BRL    | ADDR | NUM  | NUM  | BASIC   | `if (A < B) PC = Y;`            | Branch if less than |
| BRN    | ADDR | SNUM |      | BASIC   | `if (A < 0) PC = Y;`            | Branch if negative |
| BRP    | ADDR | SNUM |      | BASIC   | `if (A >= 0) PC = Y;`           | Branch if positive or zero |
| BRZ    | ADDR | NUM  |      | BASIC   | `if (A == 0) PC = Y;`           | Branch if zero |
| BLE    | ADDR | NUM  | NUM  | BASIC   | `if (A <= B) PC = Y;`           | Branch if less than or equal to |
| BNC    | ADDR | NUM  | NUM  | BASIC   | `if (A + B <= MAX) PC = Y;`     | Branch if not carry |
| BNE    | ADDR | NUM  | NUM  | BASIC   | `if (A != B) PC = Y;`           | Branch if not equal |
| BNZ    | ADDR | NUM  |      | BASIC   | `if (A != 0) PC = Y;`           | Branch if not zero |
| BOD    | ADDR | NUM  |      | BASIC   | `if (A & 0x1 == 0) PC = Y;`     | Branch if odd |
| BSL    | REG  | NUM  | NUM  | COMPLEX | `Y = A << B;`                   | Barrel shift left |
| BSR    | REG  | NUM  | NUM  | COMPLEX | `Y = A >> B;`                   | Barrel shift right |
| BSS    | REG  | SNUM | NUM  | COMPLEX | `Y = A >> B;`                   | Barrel shift right signed |
| CAL    | ADDR |      |      | BASIC   | `push(PC + 1); PC = ADDR;`      | Call |
| CPY    | PTR  | PTR  |      | BASIC   | `RAM[Y] = RAM[A];`              | Copy |
| DEC    | REG  | NUM  |      | BASIC   | `Y -= A;`                       | Decrement |
| DIV    | REG  | NUM  | NUM  | COMPLEX | `Y = A / B;`                    | Division |
| HLT    |      |      |      | BASIC   | `exit(EXIT_SUCCESS);`           | Halt |
| IMM    | REG  | CNST |      | CORE    | `Y = A;`                        | Immediate |
| IN     | REG  | PORT |      | IO      | `Y = A.get();`                  | Input |
| INC    | REG  | NUM  |      | BASIC   | `Y += A;`                       | Increment |
| JMP    | ADDR |      |      | BASIC   | `PC = Y;`                       | Jump |
| LLOD   | REG  | PTR  | NUM  | COMPLEX | `Y = RAM[A + B];`               | List load |
| LOD    | REG  | PTR  |      | CORE    | `Y = RAM[A];`                   | Load |
| LSH    | REG  | NUM  |      | BASIC   | `Y = A << 1;`                   | Left shift |
| LSTR   | PTR  | NUM  | NUM  | COMPLEX | `RAM[Y + A] = B;`               | List store |
| MLT    | REG  | NUM  | NUM  | COMPLEX | `Y = A * B;`                    | Multiply |
| MOD    | REG  | NUM  | NUM  | COMPLEX | `Y = A % B;`                    | Modulus |
| MOV    | REG  | NUM  |      | BASIC   | `Y = A;`                        | Move |
| NAND   | REG  | NUM  | NUM  | BASIC   | `Y = ~(A & B);`                 | Bitwise NAND |
| NEG    | REG  | NUM  |      | BASIC   | `Y = -A;`                       | Negate |
| NOP    |      |      |      | BASIC   | `/* nothing */;`                | No operation |
| NOR    | REG  | NUM  | NUM  | CORE    | `Y = ~(A \| B);`                | Bitwise NOR |
| NOT    | REG  | NUM  |      | BASIC   | `Y = ~A;`                       | Bitwise NOT |
| OR     | REG  | NUM  | NUM  | BASIC   | `Y = A \| B;`                   | Bitwise OR |
| OUT    | PORT | NUM  |      | IO      | `Y.put(A);`                     | Output |
| POP    | REG  |      |      | BASIC   | `Y = pop();`                    | Pop |
| PSH    | NUM  |      |      | BASIC   | `push(Y);`                      | Push |
| RET    |      |      |      | BASIC   | `PC = pop();`                   | Return |
| RSH    | REG  | NUM  |      | CORE    | `Y = A >> 1;`                   | Right shift |
| SBGE   | ADDR | SNUM | SNUM | COMPLEX | `if (A >= B) PC = Y;`           | Signed branch if greater than or equal to |
| SBLE   | ADDR | SNUM | SNUM | COMPLEX | `if (A <= B) PC = Y;`           | Signed branch if less than or equal to |
| SBRG   | ADDR | SNUM | SNUM | COMPLEX | `if (A > B) PC = Y;`            | Signed branch if greater than |
| SBRL   | ADDR | SNUM | SNUM | COMPLEX | `if (A < B) PC = Y;`            | Signed branch if less than |
| SDIV   | REG  | SNUM | SNUM | COMPLEX | `Y = A / B;`                    | Signed division |
| SETC   | REG  | NUM  | NUM  | COMPLEX | `Y = (A + B > MAX) ? MAX : 0;`  | Set if carry |
| SETE   | REG  | NUM  | NUM  | COMPLEX | `Y = (A == B) ? MAX : 0;`       | Set if equal |
| SETG   | REG  | NUM  | NUM  | COMPLEX | `Y = (A > B) ? MAX : 0;`        | Set if greater than |
| SETGE  | REG  | NUM  | NUM  | COMPLEX | `Y = (A >= B) ? MAX : 0;`       | Set if greater than or equal to |
| SETL   | REG  | NUM  | NUM  | COMPLEX | `Y = (A < B) ? MAX : 0;`        | Set if less than |
| SETLE  | REG  | NUM  | NUM  | COMPLEX | `Y = (A <= B) ? MAX : 0;`       | Set if less than or equal to |
| SETNE  | REG  | NUM  | NUM  | COMPLEX | `Y = (A != B) ? MAX : 0;`       | Set if not equal |
| SETNC  | REG  | NUM  | NUM  | COMPLEX | `Y = (A - B <= MAX) ? MAX : 0;` | Set if not carry |
| SRS    | REG  | SNUM |      | COMPLEX | `Y = A >> 1;`                   | Shift right signed |
| SSETG  | REG  | SNUM | SNUM | COMPLEX | `Y = (A > B) ? MAX : 0;`        | Signed set if |
| SSETGE | REG  | SNUM | SNUM | COMPLEX | `Y = (A >= B) ? MAX : 0;`       | Signed set if |
| SSETL  | REG  | SNUM | SNUM | COMPLEX | `Y = (A < B) ? MAX : 0;`        | Signed set if |
| SSETLE | REG  | SNUM | SNUM | COMPLEX | `Y = (A <= B) ? MAX : 0;`       | Signed set if |
| STR    | PTR  | NUM  |      | CORE    | `RAM[Y] = A;`                   | Store |
| SUB    | REG  | NUM  | NUM  | BASIC   | `Y = A - B;`                    | Subtract |
| XNOR   | REG  | NUM  | NUM  | BASIC   | `Y = ~(A ^ B);`                 | Bitwise XNOR |
| XOR    | REG  | NUM  | NUM  | BASIC   | `Y = A ^ B;`                    | Bitwise XOR |

| ARG  | INFO                                               |
| ---- | -------------------------------------------------- |
| ADDR | Instruction label or relative address              |
| CNST | Integer constant, character constant, ADDR, or PTR |
| NUM  | CNST or REG                                        |
| PORT | One of URCL ports                                  |
| PTR  | Heap address, dw label, or integer constant        |
| REG  | One of URCL registers                              |
| SNUM | Same as NUM but used as a signed value             |

## TODO

- URCL headers

- URCL ports

- URCL defined values

## Links

[URCL Documentation](https://github.com/ModPunchtree/URCL)

## License

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
```
